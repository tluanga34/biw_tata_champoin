app.controller("registration",function($scope, $http){
	var s = $scope;
	
	s.log = console.log;	
	
	function Options(optionData){
		this.all 	= optionData;
	}
	
	function Form(){
		
		
		var dropdowns = dynamicData.dropdowns,
			autoPopulate = dynamicData.autoPopulate;
			
		//CONSTRUCTOR FOR NORMAL TEXT INPUT FIELDS
		function TextInput(autoValue){
			this.value = (autoValue != undefined && autoValue != null)? autoValue : "";
			//console.log(this.value);
		}
		
		//THIS DEFINE WHEN FORM SHOULD BE DISABLED OR ENABLED
		this.disabled =  false;
		
		//CONSTRUCTOR FOR IMAGE UPLOAD FIELDS i.e Propic, Address Proof upload and Id Proof upload
		function FileImage(defaultVal){
			
			if(defaultVal && defaultVal != ""){
				this.url  = defaultVal.url;
				this.fileName = defaultVal.fileName;
				this.value = defaultVal.value;
				this.description = defaultVal.description;
			}
			else{
				this.url  = "";
				this.fileName = "";
				this.value = "";
				this.description = "";
			}
				
		}
		
		this.profilePic 	= new FileImage(autoPopulate.profilePic);
		this.idProof		= new FileImage(autoPopulate.idProof);
		this.addrProof 		= new FileImage(autoPopulate.addrProof);
		
		//CONSTRUCTOR FOR EDUCATIONAL CERTIFICATE FIELDS
		function Educertificate(defaultVal){
			this.shown = false;
			this.selected = "";
			this.arrLen = '';
			this.models = [];
			
			var _self = this;
			
			this.validate = function(){
				console.log("Validate");
				
				var models = this.models,
					error = false;
				
				models.forEach(function(key){
					console.log(key);
					if(key.fileName == "" || key.description == "" || key.description == undefined){
						key.incomplete = true;
						error = true;
					} else{
						key.incomplete = false;
					}
				});
				
				if(!error){
					this.hidePop();
					this.arrLen  = models.length;
					var plural = (models.length > 1)?'s':'';
					this.selected = models.length + " File"+plural+" Selected";
				}
			}
			
			if(defaultVal && defaultVal != ''){
				defaultVal.forEach(function(x){
					_self.models.push(new FileImage(x));
				});
				
				this.validate();
			} else{
				this.models.push(new FileImage());
			}

			this.push = function(){
				this.models.push(new FileImage());
			}			
			
			
		}
		
		Educertificate.prototype.showPop = Toggle.prototype.showPop;
		Educertificate.prototype.hidePop = Toggle.prototype.hidePop;
		
		this.eduCert 		= new Educertificate(autoPopulate.eduCert);	
		
		//CONSTRUCTOR FOR LOCATION FIELDS i.e State, District and City
		function Location(list, defaultVal){
			
			this.list 	= (list != undefined)? list : [];
			this.value = (defaultVal != undefined)? defaultVal : {};
			
			this.getOption = function(x){
								
				this.params = (x != undefined)? {geonameId: x.geonameId} : this.value;
				
				var _self = this;
				
				try{
					s.loading.show();
				} catch(e){}
				
				
				var url = "http://www.geonames.org/childrenJSON?callback=JSON_CALLBACK&style=long&noCacheIE=1482138508932&geonameId="+this.params.geonameId;

				$http.jsonp(url).success(function(data){
					_self.list = data;
					try{
						s.loading.hide();
					} catch(e){}
				});
			}
		}
		
		//CONSTRUCTOR FOR REGIONAREA i.e Region, Area office, and Distributor.
		function RegionArea(match, preList, defaultVal){	
			
			this.match = match;
			this.value = (defaultVal != undefined)?defaultVal : {};
			this.list 	= (preList != undefined)? preList : [];
			
			this.getOption = function(val){
				var _self = this,
					api = s.api.getFormOption;
				
				if(val == undefined || val == null || val == "")
					return;
				
				var data = {
					match : _self.match,
					value : val
				}
				
				s.loading.show();
				
				$http({
					url : api,
					data : data,
					method : "POST",
					headers	: {
					'Content-Type': 'application/json',
				}
				}).then(
					function(success){
						_self.list = success.data;
						s.loading.hide();
					},
					function(failed){
						s.loading.hide();
						console.log(failed.data);
					}
				);
			}
		}	
		

		function SimpleDropdown(autoValue, list){
			this.value = (autoValue != undefined && autoValue != null)? autoValue : {};
			this.list = list;
		}
		
		
		this.region 	= new RegionArea(null, dropdowns.region, autoPopulate.region);
		this.areaOffice = new RegionArea("region", dropdowns.areaOffice, autoPopulate.areaOffice);
		this.distributor = new RegionArea("apm", dropdowns.distributor, autoPopulate.distributor);
				
		this.city 		= new Location(dropdowns.city, autoPopulate.city);
		this.district 	= new Location(dropdowns.district, autoPopulate.district);
		this.state 		= new Location(dropdowns.country, autoPopulate.state);
		
		//INITIATE THE STATE DROPDOWN AND ACQUIRE LIST OF INDIAN STATES FROM API.
		this.state.getOption(autoPopulate.country);
		
		if(autoPopulate.state.geonameId && autoPopulate.state.geonameId != ''){
			this.district.getOption(autoPopulate.state);
		}
		
		if(autoPopulate.district.geonameId && autoPopulate.district.geonameId != ''){
			this.city.getOption(autoPopulate.district);
		}
		
		
		this.name 			= new TextInput(autoPopulate.name);
		this.email 			= new TextInput(autoPopulate.email);
		this.address1 		= new TextInput(autoPopulate.address1);
		this.address2 		= new TextInput(autoPopulate.address2);
		this.education 		= new TextInput(autoPopulate.education);
		
		this.dateOfBirth 		= new TextInput(autoPopulate.dateOfBirth);
		this.dateOfAnniversary 	= new TextInput(autoPopulate.dateOfAnniversary);
		this.dateOfJoining 		= new TextInput(autoPopulate.dateOfJoining);
		this.dateOfRelieving 	= new TextInput(autoPopulate.dateOfRelieving);
		this.idProofNo 			= new TextInput(autoPopulate.idProofNo);
		
		this.idProofType 		= new SimpleDropdown(autoPopulate.idProofType, dropdowns.idProofType);
		this.maritalStats 		= new SimpleDropdown(autoPopulate.maritalStats, dropdowns.maritalStats);
		this.dsrMsr 			= new SimpleDropdown(autoPopulate.dsrMsr, dropdowns.dsrMsr);
		this.addressProofType	= new SimpleDropdown(autoPopulate.addressProofType, dropdowns.addressProofType);
			
		
		//OBJECT FOR MOBILE NUMBER INPUT AND VERIFICATION.
		this.mobile = {
			value : (autoPopulate.mobile != undefined)? autoPopulate.mobile : "",
			invalidMsg : new Toggle(false),
			validate : function(inputId){
				var _self = this;
				
				if(this.value < 1000000000 || this.value > 9999999999){
					console.log("Not a mobile number");
					return;
				} else{
					
					s.loading.show();
					
					$http({
						url : s.api.checkMobile,
						params : {
							mobile : _self.value
						},
						method : "GET"
					}).then(
						function(success){
							console.log(success);
							s.loading.hide();
							
							if(success.data.status == "found"){
								_self.invalidMsg.show();
								s.form.disabled = true;
								angular.element("#"+inputId)[0].setCustomValidity("Phone Number Already Exist");
								console.log(angular.element("#"+inputId));
								
							} else{
								_self.invalidMsg.hide();
								angular.element("#"+inputId)[0].setCustomValidity("");
								s.form.disabled = false;
							}
							
						},
						function(failed){
							console.log(failed);
							s.loading.hide();
						}
					);
				}
			}
		}
		//SUBMIT THE FORM i.e SEND FORM DATA TO SERVER THROUGH AJAX.
		this.submit = function(e){
			console.log(this);
			
			var data = this;
			
			s.loading.show();
			
			$http({
				url : s.api.formAction,
				method : "POST",
				data : {
					demo : data
				},
				headers	: {
					'Content-Type': 'application/json',
				}
			}).then(
				function(sucess){
					console.log(sucess.data);
					s.loading.hide();
				},
				function(failed){
					console.log("Failed");
					s.loading.hide();
				}
			);
			e.preventDefault();
		}
		
		//SAVE THE FORM FOR LATER USE.		
		this.save = function(){
			console.log(this);
			var data = this;
			
			s.loading.show();
			
			$http({
				url : s.api.saveForm,
				method : "POST",
				data : {
					demo : data
				},
				headers	: {
					'Content-Type': 'application/json',
				}
			}).then(
				function(sucess){
					console.log(sucess.data);
					s.loading.hide();
				},
				function(failed){
					console.log("Failed");
					s.loading.hide();
				}
			);
		}
	}
	
	s.form = new Form();
	s.options = new Options(dynamicData.dropdowns);
	s.loading = new Toggle(false);
	s.api = dynamicData.config.api;
	s.userLevel = dynamicData.config.userLevel;
	
	
});