//Global Dynamic data 
var dynamicData = {
	"monthly_arr" : new Array(80, "#8dc147", "#cccccc", "10,000", "2,000", 14, 12),
	"quarterly_arc" : new Array(60, "#f59d2e", "#cccccc", "8,000", "4,000", 15, 13),
	"yearly_arc" : new Array(80, "#f75d20", "#cccccc", "10,000", "2,000", 14, 11),
	"redemption_arc" : new Array(80, "#07aa9c", "#cccccc", "10,000", "2,000", 14, 12),
	"registration_arc" : new Array(60, "#036737", "#cccccc", "10,000", "2,000", 14, 12),
	"campaign" : [
		{
			"id" : "occasional1",
			"name" : "Occasional 1",
			"bgColor" : "#94d5f1",
			"arc" : new Array(80, "#94d5f1", "#cccccc", "10,000", "2,000", 14, 12),
		},
		{
			"id" : "occasional2",
			"name" : "Occasional 2",
			"bgColor" : "#81b8de",
			"arc" : new Array(80, "#81b8de", "#cccccc", "10,000", "2,000", 14, 12),
		},
		/*{
			"id" : "occasional3",
			"name" : "Occasional 3",
			"bgColor" : "#7fa3bb",
			"arc" : new Array(60, "#7fa3bb", "#cccccc", "10,000", "2,000", 14, 12),
		},
		{
			"id" : "occasional4",
			"name" : "Occasional 4",
			"bgColor" : "#8395a1",
			"arc" : new Array(80, "#8395a1", "#cccccc", "10,000", "2,000", 14, 12),
		},
		{
			"id" : "occasional5",
			"name" : "Occasional 5",
			"bgColor" : "#9a97a8",
			"arc" : new Array(60, "#9a97a8", "#cccccc", "10,000", "2,000", 14, 12),
		},
		{
			"id" : "occasional6",
			"name" : "Occasional 6",
			"bgColor" : "#ad96aa",
			"arc" : new Array(70, "#ad96aa", "#cccccc", "10,000", "2,000", 14, 12),
		}
		*/	
	]
};

window.addEventListener("load",function() {    
	
	$('.carousel.slide').carousel();
		   
    circleMeter("monthly", dynamicData.monthly_arr);
    circleMeter("quarterly", dynamicData.quarterly_arc);  
	circleMeter("yearly", dynamicData.yearly_arc);   
	circleMeter("redemption", dynamicData.redemption_arc);   
	circleMeter("registration", dynamicData.registration_arc);  
	
	
	for(var i = 0; i < dynamicData.campaign.length; i++){
		circleMeter(dynamicData.campaign[i].id, dynamicData.campaign[i].arc); 
	}
	      
});

/*
$(window).resize(function() {  
	$('#monthly').find('svg').setAttribute('width', '100%');
	$('#quarterly').find('svg').setAttribute('width', '100%');
	$('#yearly').find('svg').setAttribute('width', '100%');
	$('#occasional1').find('svg').setAttribute('width', '100%');
	$('#occasional2').find('svg').setAttribute('width', '100%');
	$('#occasional3').find('svg').setAttribute('width', '100%');
	$('#occasional4').find('svg').setAttribute('width', '100%');
	$('#redemption').find('svg').setAttribute('width', '100%');
	$('#registration').find('svg').setAttribute('width', '100%');
	
});*/


// Raphael JS circle meter code

function circleMeter(divId, arcClass){
	var graph_wd = $('#'+divId).width();
	if(graph_wd == 0) {graph_wd = 350;}
    var r = Raphael(divId, graph_wd, 200),
        rad = 120,
        delay = 300,
        speed = 250;
    var radVal = [];
    
    r.customAttributes.arc = function (xloc, yloc, value, total, rad) {
        var alpha = 360 / total * value,
            a = (90 - alpha) * Math.PI / 180,
            x = xloc + rad * Math.cos(a),
            y = yloc - rad * Math.sin(a),
            path;
        if (total == value) {
            path = [
                ["M", xloc, yloc - rad],
                ["A", rad, rad, 0, 1, 1, xloc - 0.01, yloc - rad]
            ];
        } else {
            path = [
                ["M", xloc, yloc - rad],
                ["A", rad, rad, 0, +(alpha > 180), 1, x, y]
            ];
        }
        return {
            path: path
        };
    };

		var graph_width = ($('#'+divId).width())/2;
		if(graph_width == 0) {graph_width = 350/2;}
		//console.log(divId + " " + graph_width);
		var value = arcClass[0];
		var color = arcClass[1];
		var strokeColor = arcClass[2];
        rad -= 30;                
        $("#"+divId).find(".circleValue p").html(value+"%<br/><span>Achieved</span>").css("color",color);
		$("#"+divId).parent().find(".target .number").html(arcClass[3]).css("color",color);
		$("#"+divId).parent().find(".remaining .number").html(arcClass[4]);
		$("#"+divId).parent().find(".days").html(arcClass[5]);
		$("#"+divId).parent().find(".run").html(arcClass[6]);
		
        if(value > 100)	{value=100;	}
        var s = r.path().attr({ arc: [graph_width, 100, 100, 100, rad], 'stroke-width': 6, 'stroke': strokeColor });
        var z = r.path().attr({ arc: [graph_width, 100, 0, 100, rad], 'stroke-width': 6, 'stroke': color, 'stroke-linecap': 'round'});
        z.animate({arc: [graph_width, 100, value, 100, rad], 'stroke': color}, 1500);

}
