app.controller("tml_mapping",["$scope","$http",function(s,$http){
	
	function Data(data){
		this.list = data;
		this.filteredList = [];
		this.searchPlaceholder = "";
		this.shown = false;
		this.listSearchTerm = '';
		//Table header Label for Code
		this.desig = ""; //Guru or Champion This will be used to show different table header and column based on if he is Guru or Champion.
		
		this.empty = function(){
			this.list = [];
		};
		
		this.sort = new SORT_COLUMN();
		
		this.checkAllToggle = false;
		this.ToggleCheckAllData = function(){
			if(this.checkAllToggle){
				this.pages.value.forEach(function(key){
					if(!key.checked)
						key.checked = true;
				});
			} else {
				this.pages.value.forEach(function(key){
					if(key.checked)
						key.checked = false;
				});
			}
		}

		//SEARCH WITHIN THE TABLE//
		this.searchList = function(){
			//console.log(this.listSearchTerm);\
			var _selfData = this;
			_selfData.filteredList = [];

			if(_selfData.listSearchTerm.trim().length == 0){
				_selfData.pages.init(_selfData.list);
				return;
			}

			if(_selfData.desig == "champion") {

				_selfData.list.forEach(function(key){
					if(key.code.toString().toLowerCase().indexOf(_selfData.listSearchTerm.toLowerCase()) != -1 || key.retailer_name.toLowerCase().indexOf(_selfData.listSearchTerm.toLowerCase()) != -1 || key.firm_name.toLowerCase().indexOf(_selfData.listSearchTerm.toLowerCase()) != -1 || key.mobile.toString().toLowerCase().indexOf(_selfData.listSearchTerm.toLowerCase()) != -1){
						_selfData.filteredList.push(key);
					}
				});

			} else if(_selfData.desig == "guru"){
				console.log(_selfData.list);
				_selfData.list.forEach(function(key){
					if(key.code.toString().toLowerCase().indexOf(_selfData.listSearchTerm.toLowerCase()) != -1 || key.retailer_name.toLowerCase().indexOf(_selfData.listSearchTerm.toLowerCase()) != -1 || key.firm_name.toLowerCase().indexOf(_selfData.listSearchTerm.toLowerCase()) != -1 || key.mobile.toString().toLowerCase().indexOf(_selfData.listSearchTerm.toLowerCase()) != -1){
						_selfData.filteredList.push(key);
					}
				});
			}
		//	console.log(_selfData.filteredList);
			_selfData.pages.init(_selfData.filteredList);

		}
	}
	
	Data.prototype.show = Toggle.prototype.show;
	Data.prototype.hide = Toggle.prototype.hide;
	
	function Filter(list){		
		
		this.set = function(){
			this.value = "";
			this.list = list || [];
		};
		
		this.child = null;
		this.set();
		
		this.getList = function(match,e){

			var _self = this,
				data = {
					match : match,
					value : e
				};
						
			s.loading.show();
			
			$http({
				url : s.api.getOptions,
				method : "POST",
				data :data,
				headers : {
					"Content-Type" : "application/json"
				}
			}).then(
				function(success){
					//console.log(success.data);
					_self.list = success.data;
					_self.value = "";
					s.loading.hide();
									
					resetChild(_self.child);
					
				},
				function(failed){
					console.log(failed);
					s.loading.hide();
				}
			);			
		}	
	}
	
	function resetChild(child){
		s.search.empty();
		if(child != null){
			child.set();
			if(child.child == null){
				child.select();
			} else
				resetChild(child.child);
		}
	}
	
	s.msrDsr		= new Filter();
	s.msrDsr.select = function(){
		var msrDsr = this.value.msrDsr;
		//Reset the currently active btn indicator.
		s.tagBtnPtr = "";
		s.tagConfirm.hide();
		s.untagConfirm.hide();
		s.data.empty();
		s.data.hide();
		
		if(msrDsr == "msr"){
			s.tagGuru.show();
			s.untagGuru.show();
			s.tagChamp.hide();
			s.untagChamp.hide();
		} else if(msrDsr == "dsr"){
			s.tagGuru.hide();
			s.untagGuru.hide();
			s.tagChamp.show();
			s.untagChamp.show();
		} else if(msrDsr == "both"){
			s.tagGuru.show();
			s.untagGuru.show();
			s.tagChamp.show();
			s.untagChamp.show();
		} else {
			s.tagGuru.hide();
			s.untagGuru.hide();
			s.tagChamp.hide();
			s.untagChamp.hide();
		}
	}
	
	s.region 		= new Filter(dynamicData.filter.region);
	s.apm			= new Filter();
	s.distributor 	= new Filter();
	
	s.region.child 	= s.apm;
	s.apm.child		= s.distributor;
	s.distributor.child		= s.msrDsr;

	
	
	function TagBtn(btnName){
		this.btnName = btnName;
		this.active = false;
		this.shown = false;
	}
	
	TagBtn.prototype.show = Toggle.prototype.show;
	TagBtn.prototype.hide = Toggle.prototype.hide;
	TagBtn.prototype.getList = function(e){
	
		s.loading.show();
		
		s.tagBtnPtr = this.btnName;
		
		if(this.btnName == "tagGuru" || this.btnName == "tagChamp"){
			s.tagConfirm.show();
			s.untagConfirm.hide();
		} else{
			s.untagConfirm.show();
			s.tagConfirm.hide();
		}
		
		console.log(this.btnName);
		
		if(this.btnName == "tagGuru" || this.btnName == "untagGuru"){
			s.data.desig = "guru";
			s.data.searchPlaceholder = "Search for Guru ID, Name, Mobile or Garage name";
		} else {
			s.data.desig = "champion";
			s.data.searchPlaceholder = "Search for Champion code, Name, Mobile or Shop name..";
		}
		
		var data = {
			msrDsr : s.msrDsr.value,
			mode : this.btnName
		}
		
		s.data.hide();
		
		
		$http({
			url : s.api.getChampions,
			method : "POST",
			headers : {
				"Content-Type" : "application/json"
			},
			data : data
		}).then(
			function(success){
				s.loading.hide();
				s.data.list = success.data;
				s.data.pages = new PaginationAuto(success.data, 10, 0);
				s.data.show();
			},
			function(failed){
				s.loading.hide();
			}
		);
	}
	
	TagBtn.prototype.tagUntag = function(){
		
		var resultIds = [],
			remainders = [];


		console.log(s.data.list);

		s.data.list.forEach(function(key, index){
			if(key.checked){
				resultIds.push(key.id);
			} else {
				remainders.push(key);
			}
		});


		
		var data = {
			ids : resultIds,
			msrDsr : s.msrDsr.value,
			mode : this.btnName
		}
		
		s.loading.show();
		
		$http({
			url : s.api.tagUntag,
			data : data,
			method : "POST",
			headers : {
				"Content-Type" : "application/json"
			},
		}).then(
			function(success){
				s.data.pages.init(remainders);
				s.data.list = remainders;

				if(s.data.listSearchTerm.length != 0)
					s.data.searchList();

				s.loading.hide();
			},
			function(failed){
				console.loog("Failed");
				s.loading.hide();
			}
		);
	}
	
	
	
	
	s.tagConfirm 	= new TagBtn("tagConfirm");
	s.untagConfirm 	= new TagBtn("untagConfirm");
	
	s.tagChamp 		= new TagBtn("tagChamp");
	s.untagChamp 	= new TagBtn("untagChamp");
	s.tagGuru 		= new TagBtn("tagGuru");
	s.untagGuru 	= new TagBtn("untagGuru");
	s.tagBtnPtr		= "";
	
	s.data 			= new Data(dynamicData.list);
	s.api			= dynamicData.config.api;
	s.loading		= new Toggle(false);
	
	s.search  = {
		apiUrl : s.api.searchApi,
		msrDsr : s.msrDsr,
	}
	//console.log(s.data);
	
}]);