// 

$.fn.SalesGraphNum = function() {
	$('#sales-graph-1').highcharts({
		chart: {
			type: 'column'
		},
		title: {
			text: ''
		},
		subtitle: {
			text: ''
		},
		xAxis: {
			categories: ['Apr', 'May', 'Jun','Jul','Aug','Sep','Oct','Nov','Dec','Jan','Feb', 'Mar']
		},
		yAxis: {
			min: 0,
			title: { text: '' },
			gridLineWidth: 0
		},
		legend: {
			layout: 'horizontal',
			floating: true,
			align: 'center',
			x: 0,
			verticalAlign: 'top',
			y: 10
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			column: {
				pointPadding: 0.1,
				borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    crop: false,
					rotation: -90,
					y:-15,
					style: {"text-shadow":"none", "font-size":"12px"}
                },
				events: {
					  legendItemClick: function(e){
						  e.preventDefault();
					  }
				  }
			}
		},
		series: [{
			name: 'FY 2015',
			data: [110, 120, 130, 140, 150, 170, 175, 180, 190, 100, 210, 225],
			color: '#b3b3b3'
		}, {
			name: 'FY2016',
			data: [110, 115, 140, 150, 160, 175, 180, 190, 210, 210, 220, 240],
			color: '#4d4d4d'
		}, {
			name: 'FY2017',
			data: [130, 135, 145, 155, 165, 180, 190, 195, 210, 220, 225, 250],
			color: '#019245'
		}]
	});
};
		
$.fn.SalesGraphPer = function() {
	$('#sales-graph-1').highcharts({
		chart: {
			type: 'column'
		},
		title: {
			text: ''
		},
		subtitle: {
			text: ''
		},
		xAxis: {
			categories: ['Apr', 'May', 'Jun','Jul','Aug','Sep','Oct','Nov','Dec','Jan','Feb', 'Mar']
		},
		yAxis: {
			min: 0,
			title: { text: '' },
			gridLineWidth: 0
		},
		legend: {
			layout: 'horizontal',
			floating: true,
			align: 'center',
			x: 0,
			verticalAlign: 'top',
			y: 10
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
				'<td style="padding:0"><b>{point.y:.1f} %</b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			column: {
				pointPadding: 0.1,
				borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    crop: false,
					rotation: -90,
					y:-20,
					style: {"text-shadow":"none", "font-size":"12px"},
					formatter:function() {
                    	 return this.y +'%';
                	}
                },
				events: {
					  legendItemClick: function(e){
						  e.preventDefault();
					  }
				  }
			}
		},
		series: [{
			name: 'FY 2015',
			data: [10, 20, 30, 40, 50, 70, 75, 80, 90, 10, 110, 125],
			color: '#b3b3b3'
		}, {
			name: 'FY2016',
			data: [10, 15, 10, 50, 10, 75, 80, 90, 110, 110, 120, 140],
			color: '#4d4d4d'
		}, {
			name: 'FY2017',
			data: [30, 35, 45, 55, 65, 80, 90, 95, 110, 120, 125, 150],
			color: '#019245'
		}]
	});
};
		
		
$('#reg-graph-1').highcharts({
	chart: {
		plotBackgroundColor: null,
		plotBorderWidth: 0,
        backgroundColor:'rgba(255, 255, 255, 0.0)' 
	},
	title: {
		text: ''
	},
	tooltip: {
		pointFormat: '<b>{point.y:.0f}, {point.percentage:.1f}% </b>',
	},
	plotOptions: {
		pie: {
			center: ['50%', '50%'],
			dataLabels: {
				enabled: false,
				format: '{point.y:.0f}, <br/> {point.percentage:.1f}%',
				distance: 20
			},
			showInLegend: true
		},
		  series: {
			  point: {
				  events: {
					  legendItemClick: function(e){
						  e.preventDefault();
					  }
				  }
			  }
		  }
	},
	legend: {
		align: 'right',
		verticalAlign: 'middle',
		layout: 'vertical',
		itemStyle: {
			fontWeight: 'normal',
			fontSize: '12px'                
		},
	    labelFormat:'',
		symbolWidth: 12,
		symbolRadius: 6,
		useHTML: true,
		labelFormatter: function() {
				return '<div style=""><span>' + this.name + '</span><br/><span>' + this.y + ', '+  Math.round(this.percentage) + '%</span></div>';}
	},
	series: [{
		type: 'pie',
		name: '',
		innerSize: '70%',
		data: [
			
			{ name: 'North', y: 570, color: '#006837'},
			{ name: 'East', y: 380, color: '#8dc73f'},
			{ name: 'South', y: 380, color: '#2aabe4'},
			{ name: 'West', y: 570, color: '#00569f'}
		]
	}]
});

$('#pro-graph-1').highcharts({
	chart: {
		plotBackgroundColor: null,
		plotBorderWidth: 0,
        backgroundColor:'rgba(255, 255, 255, 0.0)' 
	},
	title: {
		text: ''
	},
	tooltip: {
		pointFormat: '<b>{point.y:.0f}, {point.percentage:.1f}% </b>',
	},
	plotOptions: {
		pie: {
			center: ['50%', '50%'],
			dataLabels: {
				enabled: false,
				format: '{point.y:.0f}, <br/> {point.percentage:.1f}%',
				distance: 20
			},
			showInLegend: true
		},
		  series: {
			  point: {
				  events: {
					  legendItemClick: function(e){
						  e.preventDefault();
					  }
				  }
			  }
		  }
	},
	legend: {
		align: 'right',
		verticalAlign: 'middle',
		layout: 'vertical',
		itemStyle: {
			fontWeight: 'normal',
			fontSize: '12px'                
		},
				  labelFormat:'',
		symbolWidth: 12,
		symbolRadius: 6,
		useHTML: true,
		labelFormatter: function() {
				return '<div style=""><span>' + this.name + '</span><br/><span>' + this.y + ', '+  Math.round(this.percentage) + '%</span></div>';}
	},
	series: [{
		type: 'pie',
		name: '',
		innerSize: '70%',
		data: [
			{ name: 'PG1', y: 250, color: '#c1282d'},
			{ name: 'PG2', y: 275, color: '#48252b'},
			{ name: 'PG3', y: 300, color: '#07693a'},
			{ name: 'PG4', y: 350, color: '#8fc644'},
			{ name: 'PG5', y: 400, color: '#27aae0'},
			{ name: 'PG6', y: 300, color: '#0562b2'},
			{ name: 'Others', y: 25, color: '#000000'}
		]
	}]
});


		
		
$('#reg-graph-2').highcharts({
	chart: {
		plotBackgroundColor: null,
		plotBorderWidth: 0,
        backgroundColor:'rgba(255, 255, 255, 0.0)' 
	},
	title: {
		text: ''
	},
	tooltip: {
		pointFormat: '<b>{point.y:.0f}, {point.percentage:.1f}% </b>',
	},
	plotOptions: {
		pie: {
			center: ['50%', '50%'],
			dataLabels: {
				enabled: false,
				format: '{point.y:.0f}, <br/> {point.percentage:.1f}%',
				distance: 20
			},
			showInLegend: true
		},
		  series: {
			  point: {
				  events: {
					  legendItemClick: function(e){
						  e.preventDefault();
					  }
				  }
			  }
		  }
	},
	legend: {
		align: 'right',
		verticalAlign: 'middle',
		layout: 'vertical',
		itemStyle: {
			fontWeight: 'normal',
			fontSize: '12px'                
		},
		labelFormat:'',
		symbolWidth: 12,
		symbolRadius: 6,
		useHTML: true,
		labelFormatter: function() {
				return '<div style=""><span>' + this.name + '</span><br/><span>' + this.y + ', '+  Math.round(this.percentage) + '%</span></div>';}
	},
	series: [{
		type: 'pie',
		name: '',
		innerSize: '70%',
		data: [
			
			{ name: 'North', y: 630, color: '#006837'},
			{ name: 'East', y: 420, color: '#8dc73f'},
			{ name: 'South', y: 420, color: '#2aabe4'},
			{ name: 'West', y: 630, color: '#00569f'}
		]
	}]
});

$('#pro-graph-2').highcharts({
	chart: {
		plotBackgroundColor: null,
		plotBorderWidth: 0,
        backgroundColor:'rgba(255, 255, 255, 0.0)'
	},
	title: {
		text: ''
	},
	tooltip: {
		pointFormat: '<b>{point.y:.0f}, {point.percentage:.1f}% </b>',
	},
	plotOptions: {
		pie: {
			center: ['50%', '50%'],
			dataLabels: {
				enabled: false,
				format: '{point.y:.0f}, <br/> {point.percentage:.1f}%',
				distance: 20
			},
			showInLegend: true
		},
		  series: {
			  point: {
				  events: {
					  legendItemClick: function(e){
						  e.preventDefault();
					  }
				  }
			  }
		  }
	},
	legend: {
		align: 'right',
		verticalAlign: 'middle',
		layout: 'vertical',
		itemStyle: {
			fontWeight: 'normal',
			fontSize: '12px'                
		},
		labelFormat:'',
		symbolWidth: 12,
		symbolRadius: 6,
		useHTML: true,
		labelFormatter: function() {
				return '<div style=""><span>' + this.name + '</span><br/><span>' + this.y + ', '+  Math.round(this.percentage) + '%</span></div>';}
	},
	series: [{
		type: 'pie',
		name: '',
		innerSize: '70%',
		data: [
			
			{ name: 'PG1', y: 280, color: '#c1282d'},
			{ name: 'PG2', y: 300, color: '#48252b'},
			{ name: 'PG3', y: 330, color: '#07693a'},
			{ name: 'PG4', y: 385, color: '#8fc644'},
			{ name: 'PG5', y: 440, color: '#27aae0'},
			{ name: 'PG6', y: 330, color: '#0562b2'},
			{ name: 'Others', y: 35, color: '#000000'}
		]
	}]
});

		
$('#reg-graph-3').highcharts({
	chart: {
		plotBackgroundColor: null,
		plotBorderWidth: 0,
        backgroundColor:'rgba(255, 255, 255, 0.0)'
		
	},
	title: {
		text: ''
	},
	tooltip: {
		pointFormat: '<b>{point.y:.0f}, {point.percentage:.1f}% </b>',
	},
	plotOptions: {
		pie: {
			center: ['50%', '50%'],
			dataLabels: {
				enabled: false,
				format: '{point.y:.0f}, <br/> {point.percentage:.1f}%',
				distance: 20
			},
			showInLegend: true
		},
		  series: {
			  point: {
				  events: {
					  legendItemClick: function(e){
						  e.preventDefault();
					  }
				  }
			  }
		  }
	},
	legend: {
		align: 'right',
		verticalAlign: 'middle',
		layout: 'vertical',
		itemStyle: {
			fontWeight: 'normal',
			fontSize: '12px'                
		},
		labelFormat:'',
		symbolWidth: 12,
		symbolRadius: 6,
		useHTML: true,
		labelFormatter: function() {
				return '<div style=""><span>' + this.name + '</span><br/><span>' + this.y + ', '+  Math.round(this.percentage) + '%</span></div>';	}
	},
	series: [{
		type: 'pie',
		name: '',
		innerSize: '70%',
		data: [
			
			{ name: 'North', y: 660, color: '#006837'},
			{ name: 'East', y: 440, color: '#8dc73f'},
			{ name: 'South', y: 440, color: '#2aabe4'},
			{ name: 'West', y: 660, color: '#00569f'}
		]
	}]
});

$('#pro-graph-3').highcharts({
	chart: {
		plotBackgroundColor: null,
		plotBorderWidth: 0,
        backgroundColor:'rgba(255, 255, 255, 0.0)' 
	},
	title: {
		text: ''
	},
	tooltip: {
		pointFormat: '<b>{point.y:.0f}, {point.percentage:.1f}% </b>',
	},
	plotOptions: {
		pie: {
			center: ['50%', '50%'],
			dataLabels: {
				enabled: false,
				format: '{point.y:.0f}, <br/> {point.percentage:.1f}%',
				distance: 20
			},
			showInLegend: true
		},
		  series: {
			  point: {
				  events: {
					  legendItemClick: function(e){
						  e.preventDefault();
					  }
				  }
			  }
		  }
	},
	legend: {
		align: 'right',
		verticalAlign: 'middle',
		layout: 'vertical',
		itemStyle: {
			fontWeight: 'normal',
			fontSize: '12px'                
		},
		labelFormat:'',
		symbolWidth: 12,
		symbolRadius: 6,
		useHTML: true,
		labelFormatter: function() {
				return '<div style=""><span>' + this.name + '</span><br/><span>' + this.y + ', '+  Math.round(this.percentage) + '%</span></div>';}
	},
	series: [{
		type: 'pie',
		name: '',
		innerSize: '70%',
		data: [
			
			{ name: 'PG1', y: 330, color: '#c1282d'},
			{ name: 'PG2', y: 350, color: '#48252b'},
			{ name: 'PG3', y: 380, color: '#07693a'},
			{ name: 'PG4', y: 400, color: '#8fc644'},
			{ name: 'PG5', y: 330, color: '#27aae0'},
			{ name: 'PG6', y: 395, color: '#0562b2'},
			{ name: 'Others', y: 15, color: '#000000'}
		]
	}]
});
    

$(function () {
    $('#total-1').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: '+ Growth over previous years',
			align: "right",
			style: {"color": "#aaaaaa"}
        },
        xAxis: {
            categories: ['FY>2015', 'FY 2015', 'FY 2016', 'FY 2017'],
            tickmarkPlacement: 'off',
            title: {
                enabled: false
            }
        },
        yAxis: {
            title: {
                text: ''
            }
        },
		legend:{
			enabled: false
		},
		labels:{
		},
        tooltip: {
            pointFormat: '<span style="color:{series.color}"></span> <b>{point.y:.0f}</b><br/>',
            shared: false
        },
        plotOptions: {
            area: {
                lineColor: '#ffffff',
                lineWidth: 1,
                marker: {
                    lineWidth: 1,
                    lineColor: '#ffffff'
                },
				dataLabels: {
                    enabled: true
                },
            }
        },
        series: [
            {
            data: [12000, 13500, 14500, 15000],
			color: '#015ba9'
        	}]
    });
});
    
	
$(function () {
    $('#total-2').highcharts({
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: ''
        },
        xAxis: [{
            categories: ['FY 2015', 'FY 2016', 'FY 2017'],
            crosshair: false
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}',
                enabled: false
            },
            title: {
                text: ''
            },
            gridLineWidth: 0
        }, { // Secondary yAxis
            title: {
                text: ''
            },
            labels: {
                format: '{value} %',
                enabled: false
            },
            gridLineWidth: 0,
            opposite: false,
            max: 300
        }],
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><br/>',
			pointFormat: '<b>{point.y}</b><br/>',
			footerFormat: '',
			shared: true,
			useHTML: true
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            x: 120,
            verticalAlign: 'top',
            y: 100,
            floating: true,
            enabled: false
        },
        
        series: [{
            name: '',
            type: 'column',
            data: [1900, 2100, 2200],
            tooltip: {
                valueSuffix: ''
            },
						color: '#ee8639',
            dataLabels: {
            enabled: true,
            crop: false,
					  style: {"text-shadow":"none", "font-size":"12px"}
                },
        },{
            name: '',
            type: 'column',
            yAxis: 1,
            data: [100, 111, 105],
            tooltip: {
                valueSuffix: '%'
            },
						color: '#ab6125',
            dataLabels: {
               enabled: true,
               crop: false,
               format: '{point.y:.0f}%',
							 style: {"text-shadow":"none", "font-size":"12px"}
                }
        }]
    });
});


$(function () {
    $('#total-3').highcharts({
		chart: {
		plotBackgroundColor: null,
		plotBorderWidth: 0
		},
		title: {
			text: ''
		},
		tooltip: {
			pointFormat: '<b>{point.y:.2f} Cr. </b>',
		},
		plotOptions: {
			pie: {
				center: ['50%', '50%'],
				dataLabels: {
					enabled: false,
					format: '{point.y:.2f} Cr.',
					distance: 20
				},
				showInLegend: true
			},
			  series: {
				  point: {
					  events: {
						  legendItemClick: function(e){
							  e.preventDefault();
						  }
					  }
				  }
			  }
		},
		legend: {
			align: 'right',
			verticalAlign: 'middle',
			layout: 'vertical',
			itemStyle: {
				fontWeight: 'normal',
				fontSize: '12px'                
			},
			labelFormat:'',
			symbolWidth: 12,
			symbolRadius: 6,
			useHTML: true,
			labelFormatter: function() {
				return '<div style=""><span>' + this.name + '</span><br/><span>' + this.y + ' Cr.</span></div>';				}
		},
		series: [{
			type: 'pie',
			name: '',
			innerSize: '70%',
			data: [
				
				{ name: 'Points Redeemed', y: 1.20, color: '#39b54a'},
				{ name: 'Points Liability', y: 1.00, color: '#00a99d'}
			]
		}]
	});
});




// 

$.fn.RedemptionAct = function() {
	$('#total-4').highcharts({
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: 0
		},
		title: {
			text: 'Member Redemption Activity'
		},
		tooltip: {
			pointFormat: '<b>{point.y:.0f}<br/>{point.percentage:.1f}%</b>',
		},
		plotOptions: {
			pie: {
				center: ['50%', '50%'],
				dataLabels: {
					enabled: false,
					format: '{point.y:.0f} <br/>{point.percentage:.0f}%',
					distance: 10
				},
				showInLegend: true
			},
			  series: {
				  point: {
					  events: {
						  legendItemClick: function(e){
							  e.preventDefault();
						  }
					  }
				  }
			  }
		},
		legend: {
			align: 'right',
			verticalAlign: 'middle',
			layout: 'vertical',
			itemStyle: {
				fontWeight: 'normal',
				fontSize: '11px'                
			},
			labelFormat:'',
			symbolWidth: 12,
			symbolRadius: 6,
			useHTML: true,
			labelFormatter: function() {
				return '<div style=""><span>' + this.name + ',</span><br/><span>' + this.y + ', '+  Math.round(this.percentage) + '%</span></div>';}
		},
		series: [{
			type: 'pie',
			name: '',
			innerSize: '70%',
			data: [
				
				{ name: 'Super Inactive (360+)', y: 10600, color: '#0060ae'},
				{ name: 'Inactive (180-360)', y: 2500, color: '#29abe2'},
				{ name: 'Dorment (90-180)', y: 1000, color: '#8cc63f'},
				{ name: 'Silent (60-90)', y: 500, color: '#006837'},
				{ name: 'Active (30-60)', y: 250, color: '#48272d'},
				{ name: 'Super Active (0-30)', y: 150, color: '#c1272d'}
			]
		}]
	});
};

$.fn.LoginAct = function() {
	$('#total-4').highcharts({
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: 0
		},
		title: {
			text: 'Member login Activity'
		},
		tooltip: {
			pointFormat: '<b>{point.y:.0f}<br/>{point.percentage:.1f}%</b>',
		},
		plotOptions: {
			pie: {
				center: ['50%', '50%'],
				dataLabels: {
					enabled: false,
					format: '{point.y:.0f} <br/>{point.percentage:.0f}%',
					distance: 10
				},
				showInLegend: true
			},
			  series: {
				  point: {
					  events: {
						  legendItemClick: function(e){
							  e.preventDefault();
						  }
					  }
				  }
			  }
		},
		legend: {
			align: 'right',
			verticalAlign: 'middle',
			layout: 'vertical',
			itemStyle: {
				fontWeight: 'normal',
				fontSize: '11px'                
			},
			labelFormat:'',
			symbolWidth: 12,
			symbolRadius: 6,
			useHTML: true,
			labelFormatter: function() {
				return '<div style=""><span>' + this.name + ',</span><br/><span>' + this.y + ', '+  Math.round(this.percentage) + '%</span></div>';}
		},
		series: [{
			type: 'pie',
			name: '',
			innerSize: '70%',
			data: [
				
				{ name: 'Super Inactive (90+)', y: 10600, color: '#0060ae'},
				{ name: 'Inactive (60-90)', y: 2500, color: '#29abe2'},
				{ name: 'Dorment (30-60)', y: 1000, color: '#8cc63f'},
				{ name: 'Silent (15-30)', y: 500, color: '#006837'},
				{ name: 'Active (7-15)', y: 250, color: '#48272d'},
				{ name: 'Super Active (0-7)', y: 150, color: '#c1272d'}
			]
		}]
	});
};
		