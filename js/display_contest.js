var app = angular.module("app",[]);

(function(){
	
//Constructor for anytype of toggle
function Toggle(state){
	this.shown = state;
}

Toggle.prototype.show = function(){this.shown = true};
Toggle.prototype.hide = function(){this.shown = false};
Toggle.prototype.toggle = function(){this.shown = !this.shown};

//Full screen gallery Constructor
function FSGallery(data){
	this.imgUrl = "";
	this.data = [];
	this.currentIndx = 0;
	this.shown = false;
	
	if(data != undefined)
		this.data = data;
}

FSGallery.prototype.show = function(indx){
	this.shown = true;
	if(indx != undefined)
		this.currentIndx = parseInt(indx);
	this.imgUrl = this.data[this.currentIndx].imgUrl;
};

FSGallery.prototype.hide = function(){
	this.shown = false;
};

FSGallery.prototype.next = function(){
	var len = this.data.length;
	this.currentIndx = (this.currentIndx >= (len-1))? 0 : ++this.currentIndx;
	this.imgUrl = this.data[this.currentIndx].imgUrl;
};

FSGallery.prototype.prev = function(){
	var len = this.data.length;
	this.currentIndx = (this.currentIndx <= 0)? (len-1) : --this.currentIndx;
	this.imgUrl = this.data[this.currentIndx].imgUrl;
};

//CONSTRCTOR FOR PAGINATIONS
function Pagination(pages, currentIndx){
	this.pages = pages;
	this.currentIndx = (currentIndx == undefined)? 0 : currentIndx;
	this.length = pages.length;
	this.value = pages[this.currentIndx].items;
}

Pagination.prototype.next = function(){
	this.currentIndx ++;
	this.value = this.pages[this.currentIndx].items;
}

Pagination.prototype.prev = function(){
	this.currentIndx--;
	this.value = this.pages[this.currentIndx].items;
}

function SELECT_COUNTER(pages){
	this.selectedIds = [];
	
	this.count = function(){
		var self = this;
		self.selectedIds = [];
		pages.forEach(function(key){
			key.items.forEach(function(key1){
				if(key1.selected == true){
					self.selectedIds.push(key1.id);
				}
			});
		});
	}
}

function SORT_COLUMN(){
	this.order = "asc";
	this.key = '';
	this.sortIt = function(dir){
		
		if(dir == this.key){
			this.key = "-"+dir; //Sort by descending
			this.order = "dsc";
		} else {
			this.key = dir;
			this.order = "asc";
		}				
	}
}

app.service("redirectTimer",function($interval){
	this.construct = function(sec, url){
		this.sec = sec;
		this.url = url;
	}
	
	this.redirect = function(){
		var self = this;
		window.location.href = self.url;
	}
	
	this.start = function(){
		var self = this;
		$interval(function(){
			if(self.sec <= 0){
				window.location.href = self.url;
			}
			else
				self.sec--;
		}, 1000);
	}
});


/*
	CONTROLLERS
*/

app.controller("referal",function($scope, $http){
	var s = $scope;
	
	s.log = function(e){console.log(e)};
	
	function Tab(isReferalLead){
		this.isReferalLead = isReferalLead;
		this.toggle = function(){
			this.isReferalLead = !this.isReferalLead
		}
	}
	
	function Form(name){
		
		//Flag to check if number has been searched by click on "check validity" button.
		var numberSearched = false,
			_selfForm		= this;
		
		this.mobile 			= new Input("referalStatus");
		this.role 				= new Input("referalStatus");
		this.referalStatus 		= new Input("referalStatus");
		this.distributorCode	= new Input("distributorCode");
		this.distributor 		= new Input("distributor",this.distributorCode);
		this.apm				= new Input("apm", this.distributor);
		this.region 			= new Input("region", this.apm);
		this.invalidReason		= new Input("invalidReason");
		this.formName			= name;
		
		//ENABLE THE FORM SO THAT USER CAN START INPUT THE FORM IF PHONE NUMBER IS NOT FOUND IN EXISTING DATA.
		this.setAsValid = function(){
			this.role.enable();
			this.distributor.enable();
			this.distributor.setOptions({});
			this.distributorCode.enable();
			this.distributorCode.setOptions({});
			this.apm.enable();
			this.apm.setOptions({});
			this.region.enable();
			this.referalStatus.setOptions(dynamicData.status);
			this.referalStatus.setExistingDropdownValue("Valid");
			this.role.setOptions(dynamicData.role);
			this.region.setOptions(dynamicData.region);
			//this.invalidReason.enable();
			this.invalidReason.reset();
			this.submitted.hide();
		}
		
		//RESET ON CLICK ON RESET BUTTON AFTER DATA SUBMITTED.
		this.reset = function(){
			this.mobile.enable();
			this.role.reset();
			this.distributor.reset();
			this.distributorCode.reset();
			this.apm.reset();
			this.region.reset();
			this.invalidReason.reset();
			this.role.reset();
			this.region.reset();
			this.invalidReason.reset();
			this.submitted.hide();
		}
		
		//DISABLE THE FORM AFTER DATA SUBMITTED.
		this.disable = function(){
			this.mobile.valid = false;
			this.role.disable();
			this.distributor.disable();
			this.distributorCode.disable();
			this.apm.disable();
			this.region.disable();
			this.invalidReason.disable();
			this.role.disable();
			this.region.disable();
			this.invalidReason.disable();
		}
		
		//CONSTRCTOR FOR INPUT
		function Input(keyname, childTofeed){
			this.valid 		= false;
			this.keyname 	= keyname;
			this.disabled 	= true;
			this.value 		= "";
			this.options 	= [];
			this.toFeed 	= (childTofeed != undefined)?childTofeed : null;
		}
		
		Input.prototype.checkValidity = function(e){
			var mobile = this.value;
			
			numberSearched = false;
			
			if(mobile <= 9999999999 && mobile >= 1000000000)
				this.valid = true;
			else{
				this.valid = false;
			}
		}
		
		Input.prototype.enable = function(){
			this.value = "";
			this.disabled = false;
		}
		
		Input.prototype.disable = function(){
			this.disabled = true;
		}
		
		Input.prototype.setExistingDropdownValue = function(value){
			this.options 	= [{name : value}];
			this.value 		= this.options[0];
			this.disabled	= true;
		}
		
		Input.prototype.setExistingValue = function(value){
			this.value 		= value;
			this.disabled	= true;
		}
		
		Input.prototype.setOptions = function(key){
			this.options = key;
		}
		
		Input.prototype.reset = function(){
			this.value = "";
			this.disabled = true;
		}
		
		//FUNCTION FOR GETTING THE FORM OPTIONS FOR THE NEXT FORM ON INPUT CHANGE EVENT
		Input.prototype.getOptions = function(){

			var self = this;
			
			if(self.toFeed == null){
				return;
			}
			
			var data = {
				match 	: self.keyname,
				value	: self.value
			}
			
			console.log("\n\nBelow is the sample of Data being post to the server\nMatch key will specify which key to match. i.e Region or APM. \nValue has name and id which can be used as a search term in the server");
			console.log(data);
			
			s.loading.show();
			
			$http({
				url : s.api.getFormOption,
				data : data,
				method : "POST",
				headers	: {
					'Content-Type': 'application/json',
				}
			}).then(
				function(sucess){
					console.log("\n\nData Returned from Server");
					console.log(sucess.data);
					s.loading.hide();
					
					self.toFeed.setOptions(sucess.data);
					
					function resetaToFeed(toFeed){
						if(toFeed){
							console.log(toFeed);
							toFeed.enable();
							
							if(toFeed.toFeed){
								resetaToFeed(toFeed.toFeed);
							}
						}
					}
					resetaToFeed(self.toFeed);
				},
				function(failed){
					s.loading.hide();
				}
			);
		}
		
		//THIS METHOD PRE-POPULATE THE EXISTING DATA FROM SERVER TO THE INPUT FORM.
		this.setExistingData = function(e){
			this.invalidReason.setExistingValue(e.invalidReason);
			this.role.setExistingDropdownValue(e.role);
			this.referalStatus.setExistingDropdownValue(e.referalStatus);
			this.distributor.setExistingDropdownValue(e.distributor);
			this.distributorCode.setExistingDropdownValue(e.distributorCode);
			this.apm.setExistingDropdownValue(e.apm);
			this.region.setExistingDropdownValue(e.region);
			this.invalidReason.disabled = false;
			this.submitted.hide();
		};
		
		//THIS METHOD SUBMIT DATA TO SERVER BY CLICK ON SUBMIT BUTTON.
		this.submit = function(e){
			
			var self = this;
			e.preventDefault();
			
			//SUBMIT DATA ONLY WHEN NUMBER HAS BEEN SEARCHED NO MATTER IF ITS EXIST OR NOT.
			if(numberSearched === true) {
				console.log("Number Searched form. Start ajax call");
				this.notSearchedMsg.hide();
				
				var data = {
					role 			: this.role.value.name,
					referalStatus 	: this.referalStatus.value.name,
					distributor		: this.distributor.value.name,
					apm				: this.apm.value.name,
					distributorCode	: this.distributorCode.value.name,
					region			: this.region.value.name,
					invalidReason	: this.invalidReason.value,
					formName		: this.formName
				}
				
				console.log(data);
				s.loading.show();
				
				$http({
					url : s.api.submitForm,
					data : data,
					method : "POST",
					headers	: {
					'Content-Type': 'application/json',
					}
				}).then(
					function(sucess){
						console.log(sucess.data);
						self.submitted.show();
						self.disable();
						s.loading.hide();
					},
					function(failed){
						console.log(failed);
						s.loading.hide();
					}
				);
				
				//NUMBER IS NOT SEARCHED. SHOW "PLEASE VALIDATE" MESSAGE.
			} else {
				console.log("Number Not Searched");
				this.notSearchedMsg.show();
			}
		};
		
		//THIS Function SEARCH FOR  A DATA WHICH IS ALREADY EXIST IN THE SYSTEM BY PHONE NUMBER
		this.searchExisting = function(e){
			var self = this,
				mobile = this.mobile.value;
						
			if(mobile <= 9999999999 && mobile >= 1000000000)
				search();
			
			self.notSearchedMsg.hide();
			
			function search(){
				s.loading.show();
				$http({
					url : s.api.searchExisting,
					params : {
						mobile : mobile
					}
				}).then(
					function(sucess){
						//console.log(sucess.data);
						if(sucess.data.status == "notFound"){
							console.log("Not found");
							self.setAsValid();
						}
						else
							self.setExistingData(sucess.data);
						
						s.loading.hide();
						numberSearched = true;						
					},
					function(failed){
						s.loading.hide();
					}
				);
			}
		}
		
		//INITIATING OBJETS.
		this.mobile.disabled = false;
		this.role.setOptions(dynamicData.role);
		this.region.setOptions(dynamicData.region);
		this.referalStatus.setOptions(dynamicData.status);
		this.notSearchedMsg = new Toggle(false);
		this.submitted 	= new Toggle(false);
	}
	
	s.tab 			= new Tab(dynamicData.config.isReferalLead);
	s.referalForm 	= new Form("ReferalForm");
	s.regnForm 		= new Form("RegnForm");
	s.api 			= dynamicData.config.api;
	s.loading 		= new Toggle(false);
	

});

app.controller("contest_participate",function($scope, $http, $timeout){
	var s 			= $scope;
	s.loading 		= new Toggle(false);
	s.errorMsg 		= new Toggle(false);
	s.successMsg 	= new Toggle(false);
	s.prompt 		= new Toggle(false);
	s.fsGallery 	= new FSGallery(dynamicData.albums);
	s.albumCTA 		= new Toggle(!dynamicData.finalSubmited);
	s.stopPropagation 	= function(e){e.stopPropagation();}
	s.api			= dynamicData.config.api;
	
	s.log = function(e){
		console.log(e)
	}
	
	function Data(dynamicData){
		this.title = dynamicData.title;
		this.desc = dynamicData.desc;
		this.album = new AlbumsUpload(dynamicData.albums);
		
		this.participate = function(){
			
			var api = dynamicData.config.api.participate,
				params = {
					userId : dynamicData.userId,
					contestId : dynamicData.contestId
				};
			
			s.loading.show();
			s.errorMsg.hide();
			s.successMsg.hide();
			
			$http({
				url : api.sendMessageTo,
				method : "POST",
				data : params,
				headers	: {
					'Content-Type': 'application/x-www-form-urlencoded',
				}
			}).then(
				function(sucess){
					s.loading.hide();
					s.successMsg.show();
					
					$timeout(function(){
						window.location.href = api.redirectOnSucess;
					},api.delayBeforeRedirect);
				},
				function(failed){
					s.loading.hide();
					s.errorMsg.show();
				}
			);
		}
		
		this.finalSubmit = function(){
			var api = dynamicData.config.api,
				params = {
					userId : dynamicData.userId,
					contestId : dynamicData.contestId
				};
			s.loading.show();
			s.prompt.hide();
			s.errorMsg.hide();
			s.successMsg.hide();
			
			$http({
				url : api.finalSubmit,
				method : "POST",
				data : params,
				headers	: {
					'Content-Type': 'application/x-www-form-urlencoded',
				}
			}).then(
				function(sucess){
					s.loading.hide();
					s.successMsg.show();
					s.albumCTA.hide();
				},
				function(failed){
					s.loading.hide();
					s.errorMsg.show();
				}
			);
		}
		
		this.editAlbum = function(){
			var api = dynamicData.config.api;
			
			window.location.href = api.editAlbum;
		}
	}
	
	function HiddenForm(){
		var self = this,
			api = dynamicData.config.api;
		
		self.inputForm = document.createElement("form");
		self.inputForm.setAttribute("action",api.formAction);
		self.inputForm.setAttribute("enctype","multipart/form-data");
		self.inputForm.setAttribute("name",api.formName);
		self.inputForm.setAttribute("method","post");
		self.inputForm.style.display = "none";
		
		self.inputFile = document.createElement("input");
		self.inputFile.setAttribute("type","file");
		self.inputFile.setAttribute("accept","image/*");
		self.inputFile.setAttribute("name",api.inputFileName);
		
		self.albumId = document.createElement("input");
		self.albumId.setAttribute("type","text");
		self.albumId.setAttribute("name",api.albumIdInputName);
		
		self.mode = document.createElement("input");
		self.mode.setAttribute("type","text");
		self.mode.setAttribute("name",api.submitInputModeName);

		self.inputForm.appendChild(self.inputFile);
		self.inputForm.appendChild(self.mode);
		self.inputForm.appendChild(self.albumId);
		document.body.appendChild(self.inputForm);
		
		self.inputFile.addEventListener("input",function(){
			console.log($scope.form);
			//self.inputForm.submit();
		});
	}
	
	function AlbumsUpload(items){
		this.items = items;
	}
	
	AlbumsUpload.prototype.delete = function(id){
		var form = $scope.form;
		form.mode.value = "Delete";
		form.albumId.value = id;
		console.log($scope.form);
		//form.inputForm.submit();
	}
	
	AlbumsUpload.prototype.addImage = function(id){
		var form = $scope.form;
		form.albumId.value = id;
		form.mode.value = "Add";
		
		form.inputFile.click();
	}
	
	s.form = new HiddenForm();
	s.data = new Data(dynamicData);
	
});

app.controller("contest_status",function($scope){
	var s = $scope;
	
	function Data(dynamicData){
		var self = this;
		
		self.construct = function(data){
			self.items = dynamicData.pages;
			
			var closeDate,
				newDate = new Date(),
				today;
						
			today 	= newDate.getFullYear() * 100;
			today 	= (today + newDate.getMonth()) * 100;
			today 	= (today + newDate.getDate());
			
			self.items.forEach(function(key){
				key.items.forEach(function(subKey){
										
					closeDate = subKey.closeDate.getFullYear() * 100;
					closeDate = (closeDate + subKey.closeDate.getMonth()) * 100;
					closeDate = (closeDate + subKey.closeDate.getDate());
										
					if(closeDate < today)
						subKey.closed = true;
					else 
						subKey.closed = false;
				});
			});
			
			self.pages = new Pagination(self.items, data.currentPage);
		}
		
		self.statusClick = function(x){
			console.log(x);
			if(!x.closed && x.finalSubmited)
				window.location.href = s.api.albumViewUrl;
			else if(!x.closed && !x.finalSubmited)
				window.location.href = s.api.albumUploadUrl;
			else if(x.closed && x.finalSubmited)
				window.location.href = s.api.albumViewUrl;
			else if(x.closed && !x.finalSubmited)
				alert("You haven't Participate in this contest");
		}
		
		this.sort = new SORT_COLUMN();
		this.sort.sortIt("contestName");
		self.construct(dynamicData);
	}
	
	s.data = new Data(dynamicData);
	s.config = dynamicData.config;
	s.api = dynamicData.config.api;
	
});

app.controller("regionWise",function($scope, $http){
	
	var s = $scope;
	
	s.data 		= new Data(dynamicData);
	s.loading 	= new Toggle(false);
	s.errorMsg 	= new Toggle(false);
	s.config	= dynamicData.config;

	function Data(dynamicData){
		
		this.region = dynamicData.region;
		
		this.construct = function(data){
			this.items = data.pages;
			this.pages = new Pagination(this.items, data.currentPage);			
		}
		
		this.construct(dynamicData);
		
		this.changeRegion = function(region){
			
			var self = this;
			
			s.errorMsg.hide();
			$scope.loading.show();
			
			$http({
				url : s.config.api.changeRegion,
				params : {
					"region" : region
				}
			}).then(
				function(sucess){
					self.region = region;
					self.construct(sucess.data);
					$scope.loading.hide();
				},
				function(failed){
					s.errorMsg.show();
					$scope.loading.hide();
				}
			);
		}
		
		this.sort = new SORT_COLUMN();
		this.sort.sortIt("dist_name");
	}	
});


app.controller("distributors",function($scope, $http, redirectTimer){
	
	var s = $scope;
	
	s.data 				= new Data(dynamicData);
	s.fsGallery 		= new FSGallery();
	s.stopPropagation 	= function(e){e.stopPropagation();}
	s.loading 			= new Toggle(false);
	s.errorMsg 			= new Toggle(false);
	s.successMsg 		= new Toggle(false);
	s.warningMsg 		= new Toggle(false);
	s.config			= dynamicData.config;
	s.timer 			= redirectTimer;
	
	s.timer.construct(5,dynamicData.redirectToAfterSave);
	
	function Data(dynamicData){
		
		this.construct = function(data){
			this.items 		= data.pages;
			this.dist_name 	= data.dist_name;
			this.id 		= data.id;
			this.area 		= data.area;
			this.pages 		= new Pagination(this.items, data.currentPage);
			this.counter 	= new SELECT_COUNTER(this.items);
		}
					
		this.shortList = function(){
			var self = this;
				selectedIds = self.counter.selectedIds,
			

			$scope.warningMsg.hide();
			$scope.errorMsg.hide();
			$scope.successMsg.hide();
			
			$scope.loading.show();
			
			$http({
				url 	: dynamicData.config.api.shortlist,
				method 	: "POST",
				params : {
					id : self.id
				},
				data 	: selectedIds,
				headers	: {
					'Content-Type': 'application/x-www-form-urlencoded',
				}
			}).then(
				function(sucess){
					
					self.items.forEach(function(key){
						key.shortListed = key.selected;
					});
					
					$scope.loading.hide();
					$scope.successMsg.show();
					
					$scope.timer.start();
				},
				function(failed){
					console.log(failed);
					$scope.loading.hide();
					$scope.errorMsg.show();
				}
			);
		}
		
		this.construct(dynamicData);
	}

});


app.controller("declare_winners",function($scope, $http, redirectTimer){
	
	var s = $scope;
	
	s.data 			= new Data(dynamicData);
	s.fsGallery 	= new FSGallery();
	s.stopPropagation = function(e){e.stopPropagation();}
	s.loading 		= new Toggle(false);
	s.errorMsg 		= new Toggle(false);
	s.successMsg 	= new Toggle(false);
	s.warningMsg	= new Toggle(false);
	s.savedMsg 		= new Toggle(false);
	s.timer 		= redirectTimer;
	
	s.timer.construct(5,dynamicData.redirectToAfterSave);
	
	function Data(dynamicData){
		
		this.construct = function(data){
			this.region = data.region;
			this.items = data.pages;
			this.pages = new Pagination(this.items, data.currentPage);
			this.counter = new SELECT_COUNTER(this.items);
		}

		this.declareWinner = function(){
			
			var self = this,
				selectedIds = self.counter.selectedIds;
						
			s.warningMsg.hide();
			s.errorMsg.hide();
			s.successMsg.hide();
			s.savedMsg.hide();
			s.loading.show();
			
			$http({
				url : dynamicData.config.api.declareWinner,
				params : {
					region : self.region
				},
				data : selectedIds,
				method : "POST",
				headers	: {
					'Content-Type': 'application/x-www-form-urlencoded',
				}
			}).then(
				function(sucess){
					self.winnerId = self.selectedId;
					
					self.items.forEach(function(key){
						key.items.forEach(function(key1){
							if(key1.selected == true){
								key1.declaredWinner = true;
							} else {
								key1.declaredWinner = false;
							}
						});
					});
					
					s.loading.hide();
					s.successMsg.show();
				},
				function(failed){
					s.loading.hide();
					s.errorMsg.show();
				}
			);
		}
		
		this.updateSelection = function(){
			
			var self = this,
				selectedIds = self.counter.selectedIds;
						
			s.warningMsg.hide();
			s.errorMsg.hide();
			s.successMsg.hide();
			s.savedMsg.hide();
			s.loading.show();
			
			$http({
				url : dynamicData.config.api.updateSelection,
				params : {
					region : self.region
				},
				data : selectedIds,
				method : "POST",
				headers	: {
					'Content-Type': 'application/x-www-form-urlencoded',
				}
			}).then(
				function(sucess){
					s.loading.hide();
					s.savedMsg.show();
					s.timer.redirect();
				},
				function(failed){
					s.loading.hide();
					s.errorMsg.show();
				}
			);
		}

		this.changeRegion = function(region){
			
			var self = this;
			
			s.warningMsg.hide();
			s.errorMsg.hide();
			s.successMsg.hide();
			s.savedMsg.hide();
			$scope.loading.show();
			
			$http({
				url : dynamicData.config.api.changeRegion,
				params : {
					"region" : region
				}
			}).then(
				function(sucess){
					self.region = region;
					self.construct(sucess.data);
					$scope.loading.hide();
				},
				function(failed){
					s.errorMsg.show();
					$scope.loading.hide();
				}
			);
		}
				
		this.construct(dynamicData);
	}
});

//CONTROLLER FOR THE WINNER PAGE
app.controller("winner",function($scope, $http){
	
	var s = $scope;
	s.loading = new Toggle(false);
	s.errorMsg = new Toggle(false);
	s.data = new Data(dynamicData);

	//console.log(s.data.page);
	
	function Data(dynamicData){
		
		var self = this;
		self.region 	= dynamicData.region;
		
		self.construct = function(champions){

			self.champ = [];
			
			champions.forEach(function(key,indx){
				self.champ.push(new Champoins(key));
			});
			
			self.page = new Pagination(self.champ);
			
		};
		
		self.construct(dynamicData.champions);
		
		function Champoins(dataItems){
			this.champ_Id 	= dataItems.champ_Id;
			this.champ_name = dataItems.champ_name;
			this.shop_name 	= dataItems.shop_name;
			this.dist_name 	= dataItems.dist_name;
			this.area 		= dataItems.area;
			this.gallery 	= new Gallery(dataItems.images);
		}
		
		function Pagination(champ){
			this.currentIndx = 0;
			this.length = champ.length;
			this.value = champ[this.currentIndx];
			
			this.next = function(){
				this.currentIndx ++;
				this.value = champ[this.currentIndx];
			}
			
			this.prev = function(){
				this.currentIndx--;
				this.value = champ[this.currentIndx];
			}
		}
		
		this.changeRegion = function(region){
			
			$scope.loading.show();
			$scope.errorMsg.hide();
			
			$http({
				url : "http://172.16.174.75/workspace/PROJECTS/Tata_motors/Tata_champoin/data/getRegionalWinner.php?",
				params : {region:region},
				method : "GET"
			}).then(
				function(sucess){
					$scope.loading.hide();
					self.region = region;
					console.log(sucess.data);
					self.construct(sucess.data.champions);
				},
				function(failed){
					$scope.loading.hide();
					$scope.errorMsg.show();
				}
			)
		};
		
		function Gallery(images){

			var currentIndx = 0;
			
			this.construct = function(images){
				currentIndx = 0;
				this.items = images;
				this.imgUrl = this.items[currentIndx].imgUrl;
				this.items[currentIndx].active = true;
			}
			
			this.construct(images);

			this.changeImage = function(indx){				
				if(indx != undefined){
					this.items[currentIndx].active = false;	
					currentIndx = parseInt(indx);
					this.imgUrl = this.items[currentIndx].imgUrl;
					this.items[currentIndx].active = true;
				}				
			}
			
			this.next = function(){
				var len = this.items.length;
				this.items[currentIndx].active = false;
				currentIndx = (currentIndx >= (len-1))? 0 : ++currentIndx;
				this.imgUrl = this.items[currentIndx].imgUrl;
				this.items[currentIndx].active = true;
			};
			this.prev = function(){
				var len = this.items.length;
				this.items[currentIndx].active = false;
				currentIndx = (currentIndx <= 0)? (len-1) : --currentIndx;
				this.imgUrl = this.items[currentIndx].imgUrl;
				this.items[currentIndx].active = true;
			};
		}
	}
});

app.directive("ngGallery",function(){
	return {
		scope : {
			ngGallery : "=",
			ngGalleryControl : "="
		},
		link : function(scope, elem, attr){
						
			var container 	= elem[0],
				data 		= scope.ngGallery,
				width		= container.clientWidth,
				eachWidth	= (width / data.length) - 1,
				widthPerc 	= (100 / data.length) - 0.3,
				difference 	= 10,
				expndPerc	= widthPerc + difference,
				squeezePerc	= (100 - expndPerc) / (data.length - 1),
				li,
				fsGallery = scope.ngGalleryControl;
			
			data.forEach(function(key,indx){
				li = document.createElement("li");
				$(li).attr({"data-id":indx});
				$(li).css({"background-image":"url('"+key.imgUrl+"')", "width":widthPerc+"%"});
				container.appendChild(li);
			});
			
			$(container.children).on("mouseover",function(){
				$(this).css({"width":expndPerc+"%"});
				$(container.children).not(this).css({"width":squeezePerc+"%"});
			});
			
			$(container).on("mouseout",function(){
				$(this.children).css({"width":widthPerc+"%"});
			});
			
			$(container.children).on("click",function(){
				var currentIndx = $(this).attr("data-id");
				fsGallery.data = data;	
				fsGallery.show(currentIndx);
				scope.$apply();
			});
		}
		
	};
});

app.directive("ngCustombind",function(){
	return {
		scope : {
			ngCustombind : "="
		},
		link : function(scope, elem, attr){			
			scope.$watch('ngCustombind', function(newValue, oldValue) {
				elem.html(scope.ngCustombind);
			});
		}
	}
});

app.directive("ngCustomvalidate",function(){
	return {
		scope : {
			ngLength : "=",
			ngLenMsg : "@",
			ngNumMsg : "@"
		},
		link : function(scope, elem, attr){
		
			var flagSet = false;
			
			setCustomMsg();
			
			elem.on("input",function(){
				setCustomMsg();
			});
			
			function setCustomMsg(){
				
				if(elem[0].value.length != scope.ngLength && !flagSet){
					elem[0].setCustomValidity(scope.ngLenMsg);
					flagSet = true;
				} else if(elem[0].value.length == scope.ngLength && flagSet){
					elem[0].setCustomValidity("");
					flagSet = false;
				}
			}
		}
	}
});


})();