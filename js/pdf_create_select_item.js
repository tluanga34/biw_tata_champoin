app

  .controller("PDFEnterTermsCtrl", [function () {
  }])

  .controller("PDFCreateSelectItemCtrl", ["$scope", "$http", "dataModel", "$timeout", function ($scope, $http, dataModel, $timeout) {


    $scope.data = dynamicData;
    $scope.loading = new dataModel.Construct(false);
    

    $scope.checkall = true;

    $scope.page = new Pagination(dynamicData.page, 0);


    $scope.page.pages.forEach(function (page) {
      page.checkall = true;
      page.sortMode = 'name';
    });


    $scope.onAllChange = function (flagValue) {
      // console.log("Change");
      // console.log(flagValue);
      // console.log($scope.page);

      $scope.page.currentPage.items.forEach(function (key) {
        key.checked = flagValue;
      });
    }

    $scope.submit = function () {


      window.location.href = "pdf_preview.html";

      $scope.loading.show();



    }

  }])



  .controller("PDFPreviewCtrl", ["$scope", "dataModel", "$timeout", function ($scope, dataModel, $timeout) {

    $scope.prompt = new dataModel.Construct(false);
    $scope.loading = new dataModel.Construct(false);
    $scope.page = dynamicData.page;

    console.log($scope.page);

    $scope.download = function () {

      $scope.loading.show();

      // $http({
      //   url: "data/dummyBackend.php",
      //   method: "POST",
      //   data: $scope.page,
      //   headers: {
      //     'Content-Type': "application/json"
      //   }
      // }).then(
      //   function(success){
      //     console.log(success.data);

      //   },
      //   function(failed){
      //     console.log(failed);
      //     $scope.loading.hide();
      //   }
      //   );


      /*
        REPLACE THIS TIMEOUT WITH ACTUAL AJAX CALL JUST LIKE ABOVE COMMENTED.
      */

      $timeout(function () {
        $scope.loading.hide();

        $scope.prompt.title = "Download Option";
        $scope.prompt.message = "PDF has been created please choose your download option";
        $scope.prompt.buttons = ["High Res for Print", "Low Res for Email"]
        $scope.prompt.show();

        $scope.userResponse = function (buttonPressed) {
          console.log(buttonPressed);
          console.log("Write a code here to download PDF");
          console.log("BUtton pressed number is $scope.prompt.buttons array index. For example 0 is Hight res, 1 is Low res");
          $scope.prompt.hide();
        }
      }, 1000);

      console.log();





    }
  }])