var app = angular.module("app",[]);


//CONSTRUCTOR SERVICE
app.service("dataModel", function () {

    this.Construct = function () {
        this.state = false;
        this.list = [];
        this.value = "";
    }

    this.Construct.prototype.hide = function () {
        this.state = false;
    }
    this.Construct.prototype.show = function () {
        this.state = true;
    }
    this.Construct.prototype.toggle = function () {
        this.state = !this.state;
    }

    this.Construct.prototype.setValue = function (value) {
        this.value = value;
    }

    this.Construct.prototype.clearValue = function () {
        this.value = "";
    }

    this.Construct.prototype.setValueFromListByKey = function (keyName) {

        var self = this;
        for (var i = 0; i < self.list.length; i++) {
            if (self.list[i][keyName] == true) {
                self.value = self.list[i];
                break;
            }
        }
    }

    this.Construct.prototype.setList = function (arr) {
        this.list = arr;
    }

    this.Construct.prototype.clearList = function () {
        this.list = [];
    }


    this.Construct.prototype.showMsg = function (msg) {
        this.value = msg;
        this.state = true;
    }

    this.Construct.prototype.hideMsg = function (msg) {
        this.value = "";
        this.state = false;
    }


});

//Constructor for anytype of toggle
function Toggle(state){
	this.shown = state;
	this.msg = "";
}

Toggle.prototype.show = function(e){
	this.shown = true
	if(e != undefined)
		this.msg = e;
};
Toggle.prototype.showPop = function(){
	this.shown = true;
	document.body.style.overflowY = "hidden";
};
Toggle.prototype.hidePop = function(){
	this.shown = false
	document.body.style.overflowY = "auto";
};
Toggle.prototype.hide = function(){this.shown = false};
Toggle.prototype.toggle = function(){this.shown = !this.shown};

//Full screen gallery Constructor
function FSGallery(data){
	this.imgUrl = "";
	this.data = [];
	this.currentIndx = 0;
	this.shown = false;
	
	if(data != undefined)
		this.data = data;
}

FSGallery.prototype.show = function(indx){
	this.shown = true;
	if(indx != undefined)
		this.currentIndx = parseInt(indx);
	this.imgUrl = this.data[this.currentIndx].imgUrl;
};

FSGallery.prototype.hide = function(){
	this.shown = false;
};

FSGallery.prototype.next = function(){
	var len = this.data.length;
	this.currentIndx = (this.currentIndx >= (len-1))? 0 : ++this.currentIndx;
	this.imgUrl = this.data[this.currentIndx].imgUrl;
};

FSGallery.prototype.prev = function(){
	var len = this.data.length;
	this.currentIndx = (this.currentIndx <= 0)? (len-1) : --this.currentIndx;
	this.imgUrl = this.data[this.currentIndx].imgUrl;
};

//CONSTRCTOR FOR PAGINATIONS
function Pagination(pages, currentIndx){
	this.pages = pages;
	this.currentIndx = (currentIndx == undefined)? 0 : currentIndx;
	this.length = pages.length;
	this.value = pages[this.currentIndx].items;
	this.currentPage = pages[this.currentIndx]
}

Pagination.prototype.next = function(){
	this.currentIndx = (this.currentIndx >= (this.length - 1))? this.currentIndx : ++this.currentIndx;
	this.value = this.pages[this.currentIndx].items;
	this.currentPage = this.pages[this.currentIndx]
}

Pagination.prototype.prev = function(){
	this.currentIndx = (this.currentIndx <= 0)? 0 : --this.currentIndx ;
	this.value = this.pages[this.currentIndx].items;
	this.currentPage = this.pages[this.currentIndx]
}

Pagination.prototype.goto = function(targetIndex){
	if(targetIndex < 0 || targetIndex > (this.length - 1))
		return;
		
	this.currentIndx = targetIndex;
	this.value = this.pages[this.currentIndx].items;
	this.currentPage = this.pages[this.currentIndx]
}

Pagination.prototype.gotoFirst = function(){
	this.currentIndx = 0;
	this.value = this.pages[this.currentIndx].items;
	this.currentPage = this.pages[this.currentIndx]
}

Pagination.prototype.gotoLast = function(){
	this.currentIndx = (this.length - 1);
	this.value = this.pages[this.currentIndx].items;
	this.currentPage = this.pages[this.currentIndx]
}

function PaginationAuto(list, interval, currentIndx){
	
	

	this.init = function(list){
		this.pages = [];
		var	items = [];

		//console.log(list);
		if(list.length == 0){
			this.value = [];
			return;
		}

		for(var i = 0; i < list.length; i++){
					
			if(items.length >= interval){
				this.pages.push({items : items});
				items = [];
				
			} else{
				items.push(list[i]);
			}
		}

		if(items.length != 0)
		this.pages.push({items : items});
		
		//console.log(this.pages);
		this.currentIndx = (currentIndx == undefined)? 0 : currentIndx;
		this.length = this.pages.length;
		this.value = this.pages[this.currentIndx].items;
	}

	this.init(list);
		
}

PaginationAuto.prototype.setRemainder = function(arr){
	
	console.log("Set remainder");
	console.log(arr);
	
	if(arr != undefined){
		this.pages[this.currentIndx].items = arr;
		this.value = this.pages[this.currentIndx].items;
	}
	
	//console.log(this.pages[this.currentIndx].items);
}


PaginationAuto.prototype.next = Pagination.prototype.next;
PaginationAuto.prototype.prev = Pagination.prototype.prev;

function SELECT_COUNTER(pages){
	this.selectedIds = [];
	
	this.count = function(){
		var self = this;
		self.selectedIds = [];
		pages.forEach(function(key){
			key.items.forEach(function(key1){
				if(key1.selected == true){
					self.selectedIds.push(key1.id);
				}
			});
		});
	}
}

function SORT_COLUMN(){
	this.order = "asc";
	this.key = '';
	this.sortIt = function(dir){
		
		if(dir == this.key){
			this.key = "-"+dir; //Sort by descending
			this.order = "dsc";
		} else {
			this.key = dir;
			this.order = "asc";
		}				
	}
}

app.directive("ngNoPropa",function() {
	return {
		restrict : "A",
		link : function (scope, $elem, $attr) {
			$elem.on("click",function(e){
				e.stopPropagation();
			});
		}
	}
});

app.service("redirectTimer",function($interval){
	this.construct = function(sec, url){
		this.sec = sec;
		this.url = url;
	}
	
	this.redirect = function(){
		var self = this;
		window.location.href = self.url;
	}
	
	this.start = function(){
		var self = this;
		$interval(function(){
			if(self.sec <= 0){
				window.location.href = self.url;
			}
			else
				self.sec--;
		}, 1000);
	}
});

app.directive("ngCustombind",function(){
	return {
		scope : {
			ngCustombind : "="
		},
		link : function(scope, elem, attr){			
			scope.$watch('ngCustombind', function(newValue, oldValue) {
				elem.html(scope.ngCustombind);
			});
		}
	}
});

//DIRECTIVE FOR INPUT DATE FUNTIONALITIES
app.directive("ngInputDate",function($timeout){
	return {
		restrict : "A",
		scope : {
			ngInputDate : "=",
			ngChange : "&"
		},
		link : function(scope, $elem, $attr){
			
			//EMPTY THE INPUT FIELD WHEN PAGE LOAD.			
			
			scope.ngInputDate = scope.ngInputDate || {};

			var id = scope.ngInputDate.id || "custid_"+Math.random(),
				dateElem;

			$elem.attr({"id":id});
				
			dateElem = new dhtmlXCalendarObject(id);
			dateElem.setDateFormat("%d-%M-%Y");
			dateElem.hideTime();
			
			if(scope.ngVal) {
				
				dateElem.setDate(new Date(scope.ngVal));
				$elem.val(dateElem.getFormatedDate("%d-%M-%Y", new Date(scope.ngVal)));
			}
			//$elem.val(scope.ngVal);

			if(scope.ngInputDate.init)
				scope.ngInputDate.init(dateElem);
						
			//LISTEN TO USER SELECT DATE EVENT AND UPDATE MODEL ACCORDINGLY
			dateElem.attachEvent("onClick", function(date, state){
				scope.ngInputDate.value = dateElem.getDate();
				scope.ngChange();
				scope.$apply();
			});
			
			//LISTENING TO OBJECT RESET REQUEST AND EMPTY THE INPUT FIELD
			scope.$watch('ngInputDate.value', function(newValue, oldValue) {

				console.log("Changed date");
				console.log(newValue);

				if(newValue == "" || newValue == undefined){
					$elem.val("");
					
					if($elem.attr("required") != undefined)
						$elem[0].setCustomValidity("Please pick a date");
				} else{
					$elem[0].setCustomValidity("");
				}
			});
				
		}
	}
});

app.directive("ngCustomvalue",function(){
	return {
		restrict : "A",
		scope : {
			ngCustomvalue : "="
		},
		link : function(scope, $elem, $attr){
			scope.$watch("ngCustomvalue",function(){
				$elem.val(scope.ngCustomvalue);
			});
		}
	}
});

app.directive("ngInputDate2",function(){
	return {
		restrict : "A",
		scope : {
			ngInputDate2 : "="
		},
		link : function(scope, $elem, $attr){
			var id = $elem.attr("id"),
				dateElem,
				scopeObj = scope.ngInputDate2;

			dateElem = new dhtmlXCalendarObject(id);
			dateElem.setDateFormat("%d-%M-%Y");
			dateElem.hideTime();

			

			scopeObj.initiate(dateElem);

			scope.$watch("ngInputDate2.value",function(){
				if(scopeObj.value != undefined && scopeObj.value != null && scopeObj.value != ''){
					console.log('Not null');
					dateElem.setDate(new Date(scopeObj.value));
					$elem.val(dateElem.getFormatedDate("%d-%M-%Y", new Date(scopeObj.value)));
				} else if(scopeObj.value == ''){
					$elem.val('');
				}
			});

			//LISTEN TO USER SELECT DATE EVENT AND UPDATE MODEL ACCORDINGLY
			dateElem.attachEvent("onClick", function(date, state){
				scopeObj.value = dateElem.getDate();
				scope.$apply();
			});
		}
	}
});

app.directive("ngCustomvalidate",function(){
	return {
		scope : {
			ngLength : "=",
			ngLenMsg : "@",
			ngNumMsg : "@"
		},
		link : function(scope, elem, attr){
		
			var flagSet = false;
			
			setCustomMsg();
			
			elem.on("input",function(){
				setCustomMsg();
			});
			
			function setCustomMsg(){
				
				if(elem[0].value.length != scope.ngLength && !flagSet){
					elem[0].setCustomValidity(scope.ngLenMsg);
					flagSet = true;
				} else if(elem[0].value.length == scope.ngLength && flagSet){
					elem[0].setCustomValidity("");
					flagSet = false;
				}
			}
		}
	}
});

//Invoke another element by jquerry selector.
app.directive("ngInvoke",function(){
	return {
		scope : {
			ngInvoke : "="
		},
		link : function(scope, elem, attr){
			
			elem.on("click",function(){
				
				var ivokeId = elem.attr("ivokeid");
				
				if(ivokeId)
					angular.element(ivokeId).click();
				else
					angular.element(scope.ngInvoke).click();
			});
			
		}
	}
});

//File change event
app.directive("ngFilechange",function(){
	return {
		scope : {
			ngFilechange : "=",
			ngModel : "="
		},
		link : function(scope, elem, attr){
			elem.on("change",function(e){
				render(e);
			});
			
			elem[0].value = "";
			
			function render(e){
				var f = e.target.files[0],
					reader = new FileReader();				

				reader.onload = (function(theFile) {
					return function(e) {
						//console.log(theFile);
						scope.ngFilechange.url = e.target.result;
						scope.ngFilechange.fileName = theFile.name;
						scope.$apply();
					};
				})(f);
				 
				reader.readAsDataURL(f);
			}
		}
	}
});

app.directive("ngAutocomplete",function(){
	return {
		scope : {
			ngAutocomplete : "="
		},
		link : function(scope, elem, attr){
			//console.log(scope);
			var scp = scope.ngAutocomplete;
			
			scp.empty = function(){
				//console.log("empty");
				//console.log(elem[0]);
				elem[0].value = "";
			}
			
			elem.autocomplete({
				source: function (request, response) {
					
					console.log(request.term);
					
					jQuery.getJSON(
						scp.apiUrl + "?term="+request.term,
						function (data) {
							data.forEach(function(key){
								key.label = key.value = key.name;
							});
							response(data);
						}
					);
				},
				minLength : 1,
				select : function(event, ui){
					console.log(ui);
					scp.msrDsr.value = ui.item;
					scp.msrDsr.select();
					//console.log(scp);
					scope.$apply();
				}
			});
		}
	}
});

app.directive("ngSearch",function(){
	return {
		scope : {
			ngSearch : "="
		},
		link : function (scope, $elem, $attr){
			
			var scopeObj = scope.ngSearch;

			scope.$watch("ngSearch.itemsListLinear",function(value){

				if(!value)
					return;
				
				value.forEach(function(key){
					key.value = key.label = key.name;
				});

				$elem.autocomplete({
					minLength: 2,
					source: value,
					select: function(event, ui) {
						scopeObj.select(ui.item);
						scope.$apply();
					}
				});
			});
		}
	}
});

app.directive("ngServersearch",function(){
	return {
		scope : {
			ngServersearch : "="
		},

		link : function(scope, $elem, $attr){
			//console.log(scope);

			$elem.autocomplete({
				minLength : 2, 
				source : scope.ngServersearch.serverSearch,
				select: function(event, ui) {
					scope.ngServersearch.select(ui.item);
					scope.$apply();
				}
			});
		}
	}
});

app.filter("bindMsrDsrName",function(){
	return function(input){
		if(input != ''){
			return input.name +" ("+input.id+")";
		}
	}
});

app.directive("ngExpanded",function(){
	return {
		scope : {
			ngExpanded : "=",

		},
		link : function (scope, $elem, $attr){

			var firstTime = true,
				expansionSpeed = 500;


			scope.$watch("ngExpanded",function(){
				
				
				if(firstTime && !scope.ngExpanded){
					$elem.css({"height":"0px"});
				}

				else if(!firstTime && scope.ngExpanded){

					setTimeout(function(){
						var height = $elem[0].scrollHeight;
						$elem.velocity({"height":height+"px"}, expansionSpeed, function(){
							$elem.css({"height":'auto'});
							
							var top = $elem[0].offsetTop - (document.body.clientHeight / 2);
							// $("html").velocity("scroll", { offset: top+"px", mobileHA: true, duration: 1000});

						});

						
					},50);		
				}

				else if(!firstTime && !scope.ngExpanded){
					$elem.velocity({"height":"0px"}, expansionSpeed);
				}


				firstTime = false;
			});
		}
	}
});

app.directive("ngHorizontalscroll",[function(){
	return {
		link : function(scope, $elem){

			//console.log($elem);
			var wheelScrollDir
			 $elem[0].addEventListener("mousewheel",doScroll);
			 $elem[0].addEventListener("DOMMouseScroll",doScroll);

			function doScroll(event){
				wheelScrollDir = event.wheelDelta || -event.detail;
				console.log(event);
				//console.log(wheelScrollDir);
				this.scrollLeft = this.scrollLeft + (wheelScrollDir);
				event.preventDefault();0
			}
			
	


		}
	}
}])
