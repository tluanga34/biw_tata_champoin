## Changes

- Fixed Internet explorer issue
- Now the ui can load the previously submitted form data. dynamicData.history will have an array of object which contain each row of submitted form. YOu can check from the sample. It could be great that not more that 5 rows are displayed to prevent vertical scroll. It can handle though  
- Validation needed for product line. 
  -Done already
- This popup has to be specific for each row.
  - This depends on dynamic json.
- Footer and head banner removed to maximize the viewport
- Filter written for part number so that selected items will not shown again.