<?php 

	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	$region = $_GET['region'];
	
	//var_dump($_SERVER);
	
	$data = array(
		"region" 	=> $region,
		"champions"	=> array(),
	);
	
	$img = 0;
	
	for($i = 0; $i <= 2; $i++) {
		
		array_push($data['champions'],array(
			"champ_Id" 	=> "Dynamic ID_".($i+1)."_".$region,
			"champ_name"=> "Dynamic Champ_".$region,
			"shop_name" => "Dynamic Shop Name_".$region,
			"dist_name" => "Dynamic Name_".$region,
			"area "		=> "Delhi",
			"images" => array()
		));
		
		for($j = 0; $j <= 8; $j++){
			array_push($data['champions'][$i]['images'], array(
				"imgUrl" => "http://172.16.174.75/workspace/PROJECTS/dummy/".$img.".jpg"
			));
			$img++;
		}
	}

	sleep(1);
	echo json_encode($data);
?>