<?php 

	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	
	$region = $_GET['region'];
	
	$data = array(
		"currentPage" => 0,
		"pages"	=> array()
	);
	
	$id = 100;
	
	for($i = 0; $i < 10; $i++){
		
		array_push($data['pages'],array(
			"items" => array()
		));
		
		for($j = 0; $j < 50; $j++){
			
			array_push($data['pages'][$i]['items'],array(
				"dist_id" => $id,
				"dist_name" => "Distributor_".$region."_".($id++),
				"reg_champions" => $id++,
				"reg_champions" => $id++,
				"part_champions" => $id++,
				"part_perc" => $id++,
				"images_uploaded" => $id++,
				"submitted" => false,
				"pending" => 4
			));	
		}
	}
	
	sleep(1);
	echo json_encode($data);
?>