<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, PATCH, DELETE');
	header('Content-Type: application/json');
	header('Access-Control-Allow-Headers: X-Requested-With,content-type');
	
	$items = array();
	
	for($i = 0; $i < 20; $i++){
		array_push($items,array(
			"id"			=> ($i+1),
			"code" 			=> ($i+1),
			"retailer_name" => "Retailer_".$i,
			"firm_name" 	=> "Firm_".$i,
			"owner_name" 	=> "Owner_".$i,
			"mobile" 		=> (9986510831 + $i),
			"address" 		=> "address_".$i,
			"state" 		=> "State_".$i,
			"district" 		=> "District_".$i,
			"pincode" 		=> "56300".$i,
		));
	}
	
	//sleep(1);
	
	echo json_encode($items);
	
?>