<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, PATCH, DELETE');
	header('Content-Type: application/json');
	header('Access-Control-Allow-Headers: X-Requested-With,content-type');
	
	$output = array();
	$msrDsr = ["msr","dsr","both"];
	$names = ["Apple","Boy","Cat","Dog","Elephant","Fan","Girl","House","Ink","Jerk","Kangaroo","Lemon","Moon","Nestle","Orbit","Pegion","Quintype","Rat","Snake"];
	$j = 0;
	$k = 0;
	
	for($i = 0; $i < 10; $i++){
		
		if($j == 3)
			$j = 0;
		if($k == 18)
			$k = 0;
		
		
		array_push($output,array(
			"type" => $names[$k],
			"name" => $names[$k],
			"id"	=> ($i+100),
			"msrDsr" => $msrDsr[$j]
		));
		
		$j++;
		$k++;
	}
	
	
	
	echo json_encode($output);
?>
