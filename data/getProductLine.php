<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, PATCH, DELETE');
	header('Content-Type: application/json');
	header('Access-Control-Allow-Headers: X-Requested-With,content-type');
	
	include("sleep.php");
	if($_SERVER["REQUEST_METHOD"] == 'POST'){
		$data = array();
		$input = json_decode(file_get_contents("php://input"),true);
		$length =  count($input);
		$i = 0;
		
		forEach($input as $key){
			//var_dump($key["id"]);
			//echo $key["id"];
			array_push($data,array(
				"id" => $i,
				"group_name" => $key['name'],
				"items" => array()
			));	
			
			for($j =0; $j < 10; $j++){
				array_push($data[$i]['items'],array(
					"id" => $j,
					"name" => $key['name']." Line Items_".$j
				));
			}
			$i++;
		}
		
		//sleep(1);
		
		echo json_encode($data);	
	}
?>